# STT Post-editing using BERT's MLM

#### Requirements:

* epitran 1.11
* fairseq 1.0.0a0 (https://github.com/pytorch/fairseq)
* torch 1.7.1
* sentencepiece 0.1.95 (https://github.com/google/sentencepiece)

#### Customized BERT architecture:

```
vibert/vibert.model
```

#### Trained model:

```
models/{domain}/checkpoint_best.pt
```

#### Usage:

```
>>> from model import PostEdit

>>> ped = PostEdit('./models/vbgh')

>>> ped.apply('trưa mai chị mới có ở nhà em đổi ngày giao hội chị em nhé')

... trưa mai chị mới có ở nhà em đổi ngày giao hộ chị em nhé
```

#### Docker:

###### Build docker image:

```
sudo docker build --pull --rm -f "Dockerfile" -t postedit:latest "."
```

###### Run docker image:

```
sudo docker run -p 5001:5001 <image_id>
```

###### Save docker image:

```
sudo docker save -o <image_id>.tar <image_id>
```

###### Load docker image:

```
sudo docker load -i <image_id>.tar
```
