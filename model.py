import os
import copy
import re
import torch
import pandas as pd
import epitran
from vibert.vibert_model import RobertaModel

class PostEdit():
    def __init__(self, model_dir='/vbgh.model'):
        pre_path = os.path.dirname(os.path.realpath(__file__))
        print(pre_path)

        # Error model
        self.tones =  {  'á' : 'B1', 'à' : 'A2', 'ả' : 'C1', 'ã' : 'C2', 'ạ' : 'B2', 
            'ấ' : 'B1', 'ầ' : 'A2', 'ẩ' : 'C1', 'ẫ' : 'C2', 'ậ' : 'B2',
            'ắ' : 'B1', 'ằ' : 'A2', 'ẳ' : 'C1', 'ẵ' : 'C2', 'ặ' : 'B2',
            'é' : 'B1', 'è' : 'A2', 'ẻ' : 'C1', 'ẽ' : 'C2', 'ẹ' : 'B2',
            'ế' : 'B1', 'ề' : 'A2', 'ể' : 'C1', 'ễ' : 'C2', 'ệ' : 'B2',
            'í' : 'B1', 'ì' : 'A2', 'ỉ' : 'C1', 'ĩ' : 'C2', 'ị' : 'B2',
            'ó' : 'B1', 'ò' : 'A2', 'ỏ' : 'C1', 'õ' : 'C2', 'ọ' : 'B2',
            'ố' : 'B1', 'ồ' : 'A2', 'ổ' : 'C1', 'ỗ' : 'C2', 'ộ' : 'B2',
            'ớ' : 'B1', 'ờ' : 'A2', 'ở' : 'C1', 'ỡ' : 'C2', 'ợ' : 'B2',
            'ú' : 'B1', 'ù' : 'A2', 'ủ' : 'C1', 'ũ' : 'C2', 'ụ' : 'B2',
            'ứ' : 'B1', 'ừ' : 'A2', 'ử' : 'C1', 'ữ' : 'C2', 'ự' : 'B2',
            'ý' : 'B1', 'ỳ' : 'A2', 'ỷ' : 'C1', 'ỹ' : 'C2', 'ỵ' : 'B2'
        }
        
        self.epi = epitran.Epitran('vie-Latn')
        
        conf_df = pd.read_csv(pre_path + '/resources/confusion.txt', header=None, delimiter='\t', encoding='utf-8')
        self.conf_dict = {}
        for i in range(len(conf_df)):
            key = str(conf_df[0].iloc[i]) + '\t' + conf_df[1].iloc[i]
            self.conf_dict[key] = conf_df[2].iloc[i]

        # Language model
        self.first = ['à', 'ừ', 'ừm', 'dạ', 'thôi', 'ủa']
        self.last = ['nhé', 'nghe', 'nha', 'nhá', 'hả', 'gì', 'nào', 'đấy', 'đây', 'đâu', 'đó', 'vậy', 'nhỉ', 'à', 'ạ']
        self.monogram_rules = ['à', 'đã', 'sẽ', 'sắp', 'không', 'đừng', 'có']
        self.bigram_rules = [
            (['ngày', 'sáng', 'trưa', 'chiều', 'tối'], ['mai', 'nay', 'ni', 'mốt', 'đó', 'hôm']),
            (['một', 'mốt', 'hai', 'ba', 'bốn', 'năm', 'lăm', 'sáu', 'bảy', 'tám', 'chín', 'mười', 'mươi'], ['giờ', 'phút']),
            (['giờ'], ['sáng', 'trưa', 'chiều', 'tối']),
            (['cho'], ['ai', 'anh', 'em', 'chị', 'tôi', 'tui', 'tao', 'mày', 'nó', 'họ']),
            (['sao'], ['vậy', 'nhỉ', 'anh', 'em', 'chị']),
            (['hôm', 'tuần'], ['nay', 'này', 'ni', 'trước', 'sau']),
            (['cả'], ['ngày', 'tuần']),
        ]
        self.site_rules = ['tỉnh', 'phố', 'huyện', 'xã', 'phường', 'thôn', 'làng', 'xóm', 'tổ', 'đường', 'số']
        self.name_rules = ['nguyễn', 'trần', 'lê', 'phạm', 'huỳnh', 'phan', 'ngô', 'hồ', 'võ']
        
        self.model = RobertaModel.from_pretrained(
            pre_path + model_dir,
            checkpoint_file='checkpoint_best.pt',
            bpe='sentencepiece'
        )
        self.model.eval()
        if torch.cuda.is_available():
            self.model.cuda()

        # General variables
        self.q = 1
    
    def decompose(self, word):
        ipa = self.epi.transliterate(word).replace('ɛu', 'ew').replace('eo', 'ɛw').replace('ɛɲ', 'aɲ')
        p1 = ''; p2 = ''; p3 = ''

        if ipa[0] not in ['a', 'ă', 'ɤ̆', 'ɛ', 'e', 'i', 'ɔ', 'o', 'ɤ', 'u', 'ɯ', 'ə']:
            if word.startswith('r'):
                p1 = 'r'
            elif word.startswith('s'):
                p1 = 'ʂ'
            elif word.startswith('tr'):
                p1 = 'ʈ'
            elif word.startswith('th'):
                p1 = ipa[:2]
            else:
                p1 = ipa[0]
            ipa = ipa[len(p1):]
        
        if ipa!='' and (ipa[-1] not in ['a', 'ă', 'ɤ̆', 'ɛ', 'e', 'i', 'ɔ', 'o', 'ɤ', 'u', 'ɯ', 'ə']):
            p3 = ipa[-1]
            ipa = ipa[:-1]
        
        p2 = ipa
        if p2=='wa' and 'ua' in word:
            p2 = 'uə'
        
        return p1, p2, p3

    def p4_analyze(self, word):
        '''
        Tone analysis
        '''
        for c in word:
            if c in self.tones:
                return self.tones[c]
        return 'A1'

    def possibility(self, word1, word2):
        phonemes1 = self.decompose(word1)
        phonemes2 = self.decompose(word2)

        P = 1
        for i in range(3):
            key = str(i + 1) + '\t' + phonemes1[i] + '_' + phonemes2[i]
            p = self.conf_dict[key] if key in self.conf_dict else 0
            P *= p
        
        key = '4\t' + self.p4_analyze(word1) + '_' + self.p4_analyze(word2)
        P *= self.conf_dict[key]
        
        return P

    def apply(self, sentence, num_candidates=30, theta=1e-5):
        """
        Apply post-editing to a sentence.
        
        Input:
            - sentence: original sentence
            - num_candidates: number of candidates (the lower the faster; the higher the better)
            - theta: threshold of sensitivity (the higher the more different between post sentence and original sentence; default 1e-5 is sweet spot)
        
        Output: post-edited sentence
        """

        token_list = sentence.split(' ')
        justified_token_list = copy.deepcopy(token_list)
        chosen_score = 0

        frozen_indexes = set()
        
        if token_list[0] in self.first:
            frozen_indexes.add(0)
        if token_list[-1] in self.last or token_list[-1] in self.monogram_rules:
            frozen_indexes.add(len(token_list) - 1)
        for i in range(len(token_list) - 1):
            if i in frozen_indexes:
                continue
            for rule in self.bigram_rules:
                if token_list[i] in rule[0] and token_list[i+1] in rule[1]:
                    frozen_indexes.update([i, i+1])
                    break
            if token_list[i] in self.monogram_rules:
                frozen_indexes.add(i)
        
        for i in range(len(token_list)-1):
            if token_list[i] in self.site_rules:
                frozen_indexes.update(range(i, i + 4))
            elif token_list[i] in self.name_rules:
                frozen_indexes.update(range(i, i + 3))
        

        for i in range(len(token_list)):
            original_word = token_list[i]

            if (i in frozen_indexes):
                continue
            
            # prepare masked line
            masked_token_list = copy.deepcopy(token_list)
            masked_token_list[i] = '<mask>'
            masked_line = ' '.join(masked_token_list)

            # find candidates
            top_candidates = self.model.fill_mask(masked_line, topk=num_candidates)
            original_score = 0; max_score = 0; argmax = ('', 0, '')
            
            for candidate in top_candidates:
                _sentence, _possibility, _word = candidate
                if (re.match(r' [^\d\W]+$', _word)): _word = _word[1:].lower()
                else: continue

                _score = (_possibility ** (2-self.q)) * (self.possibility(_word, original_word) ** self.q)
                
                if _word == original_word:
                    original_score = _score
                    break
                
                if _score > max_score:
                    max_score = _score
                    argmax = _sentence, _word

            if (max_score - original_score > theta) and (max_score / (original_score + theta) > 2) \
            and (max_score - original_score > chosen_score):
                chosen_token = i, argmax[1]
                chosen_score = max_score - original_score
        
        if (chosen_score > 0):
            justified_token_list[chosen_token[0]] = chosen_token[1]
        
        justified_sentence = ' '.join(justified_token_list)

        return justified_sentence

    def remove_stutter(self, sentence):
        token_list = sentence.split(' ')
        collapsed_tokens = [(token_list[0], 0)]
        
        for i, token in enumerate(token_list):
            if token == collapsed_tokens[-1][0]: # stutter
                collapsed_tokens[-1] = (collapsed_tokens[-1][0], collapsed_tokens[-1][1] + 1)
            else: # new token
                collapsed_tokens.append((token, 1))
        
        return ' '.join([t[0] for t in collapsed_tokens])
    
