from typing import List
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from model import PostEdit


class TextResBody(BaseModel):
    data: List[str]


# initialize fast api application
app = FastAPI()

# voicebot giao hàng
ped_vbgh = PostEdit('/models/vbgh')

# fe telesale
ped_fets = PostEdit('/models/fets')


@app.post("/post-editing/{item_id}/")
async def post_edit(item_id: str, request: TextResBody, lookup: int = 30, threshold: float = 1e-5):

    # Finalize results
    
    final_results = []
    for text in request.data:
        if item_id=='vbgh':
            global ped_vbgh
            edited_text = ped_vbgh.apply(text, num_candidates=lookup, theta=threshold)
        elif item_id=='fets':
            global ped_fets
            edited_text = ped_fets.apply(text, num_candidates=lookup, theta=threshold)

        final_results.append({
            "original": text,
            "postedit": edited_text
        })

    return {"model": item_id, "data":final_results}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5001)