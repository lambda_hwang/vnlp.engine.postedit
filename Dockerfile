FROM python:3.7-buster

# Create root directory for support .env file
RUN mkdir -p /root

# Create a folder for containing the source code
WORKDIR /root
RUN mkdir -p /root/source
WORKDIR /root/source

# COPY ./vnlp-ge/requirements.txt requirements.txt
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Copy source from host into docker enviroment
COPY . .

# Create /configs folder for .env file
WORKDIR /root
RUN mkdir -p configs

EXPOSE 5001

# Start service
WORKDIR /root/source
CMD ["gunicorn", "--bind", "0.0.0.0:5001", "-k", "uvicorn.workers.UvicornWorker", "main:app"]